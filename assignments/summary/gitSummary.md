**What is git?**

1. It is a open source version controlled system 
2. In my opinion, it is used to basically track whatever changes you have made in your _project_ or _folder_ containing the project work. 
3. It is also used to _push_ and _pull_(which will be discussed later) changes from remote repositories like say Gitlab 


**Required vocabulary**

1. Repository: A collection of files and folders that you’re using git to track.

2. Commit: When you save your work to your repository, you are _**committing**_ to it.

3. Push: syncing your commits to the cloud (i.e. Github).
    * Advantageous when you work offline


4. Branch: You can use this concept to modify a particular _**module**_ of a code without disturbing its integrity.

5. Clone: It takes the entire repository and makes an exact copy of it on your computer.

6. Fork: Similar to clone but you get an entirely new repo of that code under your own name. 